const { PythonShell } = require('python-shell')
const express = require('express');
const socketio = require('socket.io');
const app = express();

const port = 3001;

class Compiler {
    template = "OUTPUT = 0\n" +
        "INPUT = 1\n" +
        "LOW = 0\n" +
        "HIGH = 1\n" +
        "def pinMode(pin, mode):\n" +
        "\tprint(f'pinMode-{pin}:mode-{mode}')\n\n" +
        "def digitalWrite(pin, level):\n" +
        "\tprint(f'digitalWrite-{pin}:level-{level}')\n\n" +
        "def delay(miliseconds):\n" +
        "\tprint(f'delay-{miliseconds}')\n\n"

    mainFunction = "print('start')\nsetup()\nprint('^loop')\nloop()"
}

const server = app.listen(port, () => {
    console.log("Listening on port " + port);
});

const io = socketio(server);

io.on('connection', (socket) => {
    console.log("Connected");

    socket.on('code', (data) => {
        let compiler = new Compiler();

        PythonShell.runString((compiler.template + data + compiler.mainFunction), null, (err, output) => {
            if (err) socket.emit("code", "Compilation error");
            else socket.emit("code", output.join('\n'));
        });
    });
});